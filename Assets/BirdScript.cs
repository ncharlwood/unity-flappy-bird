using UnityEngine;
using System;

public class BirdScript : MonoBehaviour {

    public new Rigidbody2D rigidbody2D;
    public LogicManagerScript logic;
    public AudioSource[] deathAudioArray;
    public SpriteRenderer spriteRenderer;
    public Sprite deadSprite;
    public GameObject explosion;
    public Animator haha;

    public float flapStrength = 9;
    public float screenLimit = 4.5F;
    public bool isAlive = true;

    void Start() {
        deathAudioArray = GameObject.FindGameObjectWithTag("DeathAudio").GetComponents<AudioSource>();
    }

    void Update() {

        if (isAlive && Math.Abs(transform.position.y) > screenLimit) {
            killBird();
        }

        if (Input.GetKeyDown(KeyCode.Space) && isAlive) {
            rigidbody2D.velocity = flapStrength * Vector2.up;
        }
    }

    public void OnCollisionEnter2D(Collision2D other) {
        Vector3 explosionPosition = other.GetContact(0).point;
        explosionPosition.z += 50;
        Instantiate(explosion, explosionPosition, transform.rotation);
        killBird();
    }

    public void killBird() {
        if (!isAlive) {
            // stop hes already dead
            return;
        }

        haha.speed = 0.5F;

        logic.getRandomAudio(deathAudioArray).Play();

        spriteRenderer.sprite = deadSprite;

        isAlive = false;
        logic.gameOver();
    }
}
