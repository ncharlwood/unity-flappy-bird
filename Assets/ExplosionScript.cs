using UnityEngine;

public class ExplosionScript : MonoBehaviour {
    
    public PipeScript pipes;

    void Start() {
        pipes = GameObject.FindGameObjectWithTag("Pipe").GetComponent<PipeScript>();
    }

    void Update() {
        pipes.moveWithPipe(gameObject);
    }

    public void Create(Vector3 position) {
        gameObject.SetActive(true);
        transform.position = new Vector3(position.x, position.y, position.z + 50);
    }
}
