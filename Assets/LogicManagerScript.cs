using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LogicManagerScript : MonoBehaviour {


    public AudioSource[] pointsAudioArray;
    public GameObject gameOverScreen;

    public int playerScore;
    public Text scoreText;
    public float flashLength = 0.2F;
    public float flashTimer = 0;
    public int flashCount = 0;

    void Start() {
        pointsAudioArray = GameObject.FindGameObjectWithTag("PointsAudio").GetComponents<AudioSource>();
    }

    void Update() {

        if (flashCount == 0) {
            return;
        }
        
        if (flashTimer < flashLength) {
            flashTimer += Time.deltaTime;
        } else {
            flashTimer = 0;
            flashCount = (flashCount + 1) % 4;
            scoreText.color = scoreText.color == Color.white ? Color.yellow : Color.white;
        }

    }

    [ContextMenu("Add Score")]
    public void addScore() {
        playerScore++;

        scoreText.text = playerScore.ToString();

        if (playerScore % 10 == 0) {
            getRandomAudio(pointsAudioArray).Play();
            scoreText.color = Color.yellow;
            flashCount++;

        }
    }

    public void restartGame() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    
    public void gameOver() {
        gameOverScreen.SetActive(true);
    }

    public AudioSource getRandomAudio(AudioSource[] audioArray) {

        int length = audioArray.GetLength(0);

        // mod by length since rand.value is inclusive and can return 1
        // which would give us array out of bounds
        int randomIndex = (int)(length * UnityEngine.Random.value) % length;

        return audioArray[randomIndex];
    }

}
