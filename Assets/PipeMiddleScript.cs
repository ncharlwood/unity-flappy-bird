using UnityEngine;

public class PipeMiddleScript : MonoBehaviour {

    public LogicManagerScript logic;

    void Start() {
        logic = GameObject.FindGameObjectWithTag("Logic").GetComponent<LogicManagerScript>();
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (
            other.gameObject.name == "Bird" &&
            other.gameObject.layer == 3 &&
            other.gameObject.GetComponent<BirdScript>().isAlive
        ) {
           logic.addScore();
        }
    }
}
