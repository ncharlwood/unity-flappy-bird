using UnityEngine;

public class PipeScript : MonoBehaviour {

    public float moveSpeed = 5;

    void Update() {
        moveWithPipe(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other) {
        Debug.Log("on trigger enter");
    }

    void OnCollisionEnter2D (Collision2D col) {
        Debug.Log("on collision enter");
    }

    public void moveWithPipe(GameObject movingObject) {
        movingObject.transform.position += Vector3.left * moveSpeed * Time.deltaTime;

        if (movingObject.transform.position.x < -15) {
            Destroy(movingObject);
        }
    }
}
