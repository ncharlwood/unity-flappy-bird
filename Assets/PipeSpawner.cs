using UnityEngine;

public class PipeSpawner : MonoBehaviour {

    public GameObject pipe;

    public float spawnRate = 2;
    private float spawnTimer = 0;

    public float heightOffset = 2.5F;
 
    void Start() {
       spawnPipe();
    }

    void Update() {
        if (spawnTimer < spawnRate) {
            spawnTimer += Time.deltaTime;
        } else {
            spawnPipe();
        }
    }

    void spawnPipe() {
        float yLowerLimit = transform.position.y - heightOffset;
        float yHigherLimit = transform.position.y + heightOffset;

        float yValue = Random.Range(yLowerLimit, yHigherLimit);

        Vector3 newPosition = new Vector3(transform.position.x, yValue, 100);
        Instantiate(pipe, newPosition, transform.rotation);
        spawnTimer = 0;
    }
}
